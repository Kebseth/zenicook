package com.example.zenicook.repository;

import com.example.zenicook.model.Recipes;
import org.springframework.data.repository.CrudRepository;

public interface RecipesRepository extends CrudRepository<Recipes, String> {

}
