package com.example.zenicook.representation.mesure;

public class MesureRepresentation {

    private Integer quantity;
    private String ingredient;
    private String unit;

    protected MesureRepresentation () {}
    public MesureRepresentation (Integer quantity, String ingredient) {
        this.quantity = quantity;
        this.ingredient = ingredient;
    }

    public Integer getQuantity() { return quantity; }
    public String getIngredient() { return ingredient; }
    public String getUnit() { return unit; }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }
}
