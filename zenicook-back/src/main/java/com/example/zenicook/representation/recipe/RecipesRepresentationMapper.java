package com.example.zenicook.representation.recipe;

import com.example.zenicook.model.Recipes;
import com.example.zenicook.representation.mesure.MesureMapper;
import com.example.zenicook.representation.mesure.MesureRepresentation;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class RecipesRepresentationMapper {

    MesureMapper mesureMapper;

    public RecipesRepresentationMapper(MesureMapper mesureMapper) {
        this.mesureMapper = mesureMapper;
    }

    public RecipesRepresentation mapToRecipe (Recipes recipe) {
        Set<MesureRepresentation> setOfDisplayableMesure = mesureMapper.getDisplayableMesure(recipe.getRecipeContent());
        return new RecipesRepresentation(recipe.getId(), recipe.getName(), setOfDisplayableMesure);
    }
}
