package com.example.zenicook.representation.ingredient;

import com.example.zenicook.model.Ingredient;
import org.springframework.stereotype.Component;

@Component
public class IngredientRepresentationMapper {
    public IngredientRepresentation mapToIngredient (Ingredient ingredient) {
        return new IngredientRepresentation(ingredient.getId(), ingredient.getName());
    }
}
