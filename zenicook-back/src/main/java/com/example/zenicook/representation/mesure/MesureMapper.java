package com.example.zenicook.representation.mesure;

import com.example.zenicook.beans.Convert;
import com.example.zenicook.model.Mesure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class MesureMapper {

    private Convert convert;

    @Autowired
    public MesureMapper(Convert convert) {
        this.convert = convert;
    }

    public Set<MesureRepresentation> getDisplayableMesure(Set<Mesure> mesure){

        return mesure.stream().map(individualMesure ->
        {       MesureRepresentation representation = new MesureRepresentation();
            representation.setQuantity(individualMesure.getQuantity());
            representation.setIngredient(convert.convertIdInIngredientName(individualMesure.getId().getIngredient_id()));
                if (individualMesure.getUnit() != null) {
                    representation.setUnit(individualMesure.getUnit());
                } return representation;
        })
                .collect(Collectors.toSet());
    }

    public Convert getConvert() { return convert; }

    public void setConvert(Convert convert) {
        this.convert = convert;
    }
}
