package com.example.zenicook.representation.mesure;

import com.example.zenicook.model.Ingredient;

public class NewMesureRepresentation {

    private Integer quantity;
    private String unit;
    private String ingredient;

    public NewMesureRepresentation () {}

    public NewMesureRepresentation (Integer quantity, String ingredient, String unit) {
        this.quantity   = quantity;
        this.unit       = unit;
        this.ingredient = ingredient;
    }

    public Integer getQuantity() { return quantity; }
    public String getIngredient() { return ingredient; }
    public String getUnit() { return unit; }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }


    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }
}
