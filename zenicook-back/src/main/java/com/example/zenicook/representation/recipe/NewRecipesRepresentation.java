package com.example.zenicook.representation.recipe;

import com.example.zenicook.representation.mesure.NewMesureRepresentation;

import java.util.Set;

public class NewRecipesRepresentation {
    private String name;
    private Set<NewMesureRepresentation> listOfMesures;

    public NewRecipesRepresentation(String name, Set<NewMesureRepresentation> listOfMesures) {
        this.name = name;
        this.listOfMesures = listOfMesures;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<NewMesureRepresentation> getListOfIngredients() {
        return listOfMesures;
    }

    public void setListOfIngredients(Set<NewMesureRepresentation> listOfIngredients) {
        this.listOfMesures = listOfIngredients;
    }
}
