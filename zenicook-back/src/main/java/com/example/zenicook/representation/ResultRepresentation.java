package com.example.zenicook.representation;

public class ResultRepresentation {
    private boolean result;

    public ResultRepresentation (boolean result) {
        this.result = result;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
