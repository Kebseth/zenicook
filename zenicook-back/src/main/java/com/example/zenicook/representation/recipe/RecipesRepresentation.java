package com.example.zenicook.representation.recipe;

import com.example.zenicook.representation.mesure.MesureRepresentation;

import java.util.Set;

public class RecipesRepresentation {
    private String id;
    private String name;
    private Set<MesureRepresentation> listOfMesures;

    public RecipesRepresentation(String id, String name, Set<MesureRepresentation> listOfMesures) {
        this.id = id;
        this.name = name;
        this.listOfMesures = listOfMesures;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MesureRepresentation> getListOfMesures() {
        return listOfMesures;
    }

    public void setListOfMesures(Set<MesureRepresentation> listOfMesures) {
        this.listOfMesures = listOfMesures;
    }

}