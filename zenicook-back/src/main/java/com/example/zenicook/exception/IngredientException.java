package com.example.zenicook.exception;

public class IngredientException extends Throwable {
    private String text;

    public IngredientException(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
