package com.example.zenicook.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class MesureKey implements Serializable {

    @Column(name = "ingredient_id")
    String ingredient_id;

    @Column(name = "recipe_id")
    String recipe_id;

    public MesureKey() {}
    public MesureKey(String ingredient_id, String recipe_id) {
        this.ingredient_id = ingredient_id;
        this.recipe_id     = recipe_id;
    }

    public String getIngredient_id() { return ingredient_id; }
    public String getRecipe_id() { return recipe_id; }

    public void setIngredient_id(String ingredient_id) { this.ingredient_id = ingredient_id; }

    public void setRecipe_id(String recipe_id) { this.recipe_id = recipe_id; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MesureKey)) return false;
        MesureKey that = (MesureKey) o;
        return Objects.equals(getIngredient_id(), that.getIngredient_id()) &&
                Objects.equals(getRecipe_id(), that.getRecipe_id());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIngredient_id(), getRecipe_id());
    }
}
