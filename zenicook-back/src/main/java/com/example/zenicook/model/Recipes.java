package com.example.zenicook.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "recipes")
public class Recipes {
    @Id
    @Column(name = "id")
    private String id;
    private String name;
    /*@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "mesures",
            joinColumns = @JoinColumn(name = "recipe_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "ingredient_id", referencedColumnName = "id")
    )*/
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "recipe")
    private Set<Mesure> listOfMesures = new HashSet<>();



    public Recipes (String id, String name, Set<Mesure> listOfMesures) {
        this.id            = id;
        this.name          = name;
        this.listOfMesures = listOfMesures;
    }

    protected Recipes () {}

    public Set<Mesure> getRecipeContent() { return listOfMesures; }
    public String getName() { return name; }
    public String getId() { return id; }

    public void setRecipeContent(Set<Mesure> listOfMesures) {
        this.listOfMesures = listOfMesures;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }
}
