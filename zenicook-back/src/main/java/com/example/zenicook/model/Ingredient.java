package com.example.zenicook.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ingredient")
public class Ingredient {
    @Id
    @Column(name = "id")
    private String id;
    private String name;

    @OneToMany(mappedBy = "ingredient")
    private Set<Mesure> listOfMesures = new HashSet<>();

    protected Ingredient () {}

    public Ingredient (String id, String name) {
        this.id   = id;
        this.name = name;
    }

    public Set<Mesure> getListOfMesures() {
        return listOfMesures;
    }

    public void setListOfMesures(Set<Mesure> listOfMesures) {
        this.listOfMesures = listOfMesures;
    }
    public String getId() { return id; }
    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public void setId(String id) { this.id = id; }
}
