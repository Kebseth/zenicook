package com.example.zenicook.model;

import javax.persistence.*;

@Entity
@Table(name = "mesures")
public class Mesure {

    @EmbeddedId
    MesureKey id;
    private Integer quantity;
    private String unit;

    @ManyToOne
    @JoinColumn(name = "recipe_id", updatable = false, insertable = false)
    private Recipes recipe;

    @ManyToOne
    @JoinColumn(name = "ingredient_id", updatable = false, insertable = false)
    private Ingredient ingredient;

    protected Mesure () {}

    public Mesure (MesureKey id, String unit, Integer quantity) {
        this.id        = id;
        this.quantity   = quantity;
        this.unit       = unit;

    }

    public Integer getQuantity() { return quantity; }
    public String getUnit() { return unit; }
    public MesureKey getId() { return id; }

    public void setId(MesureKey id) { this.id = id; }

    public void setQuantity(Integer quantity) { this.quantity = quantity; }

    public void setUnit(String unit) { this.unit = unit; }
}
