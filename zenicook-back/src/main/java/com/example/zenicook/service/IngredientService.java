package com.example.zenicook.service;

import com.example.zenicook.beans.Counter;
import com.example.zenicook.exception.IngredientException;
import com.example.zenicook.model.Ingredient;
import com.example.zenicook.repository.IngredientRepository;
import com.example.zenicook.representation.ingredient.NewIngredientRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class IngredientService {
    private Counter counter;
    private IngredientRepository ingredientRepository;

    @Autowired
    public IngredientService(Counter counter, IngredientRepository ingredientRepository) {
        this.counter              = counter;
        this.ingredientRepository = ingredientRepository;
    }

    public List<Ingredient> getAllIngredient() {
        return (List<Ingredient>) ingredientRepository.findAll();
    }

    public Ingredient addAnIngredient(NewIngredientRepresentation ingredientRepresentation) throws IngredientException {
        if(ingredientRepository.findByName(ingredientRepresentation.getName()).isEmpty()) {
            Ingredient ingredient = new Ingredient(counter.getId(), ingredientRepresentation.getName() );
            return ingredientRepository.save(ingredient);
        } else throw new IngredientException("Ingredient is already existing (noob)");
    }

    public Ingredient updateAnIngredient(String id, NewIngredientRepresentation ingredient) {
        Optional<Ingredient> referenceIngredient = ingredientRepository.findById(id);
        if(referenceIngredient.isPresent()) {
            if (ingredient.getName() != null) { referenceIngredient.get().setName(ingredient.getName()); }
            return ingredientRepository.save(referenceIngredient.get());
        }
        else return new Ingredient(id, ingredient.getName());
    }

    public Boolean nameExists(String ingredientName) {
        return this.ingredientRepository.findByName(ingredientName).isPresent();
    }
}
