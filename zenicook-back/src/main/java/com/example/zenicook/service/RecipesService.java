package com.example.zenicook.service;

import com.example.zenicook.beans.Counter;
import com.example.zenicook.model.Ingredient;
import com.example.zenicook.model.Mesure;
import com.example.zenicook.model.MesureKey;
import com.example.zenicook.model.Recipes;
import com.example.zenicook.repository.IngredientRepository;
import com.example.zenicook.repository.RecipesRepository;
import com.example.zenicook.representation.ingredient.NewIngredientRepresentation;
import com.example.zenicook.representation.mesure.NewMesureRepresentation;
import com.example.zenicook.representation.recipe.NewRecipesRepresentation;
import com.example.zenicook.representation.recipe.RecipesRepresentationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Component
public class RecipesService {
    private RecipesRepository recipesRepository;
    private Counter counter;
    private RecipesRepresentationMapper recipesRepresentationMapper;
    private IngredientService ingredientService;
    private IngredientRepository ingredientRepository;

    @Autowired
    public RecipesService(RecipesRepository recipesRepository, Counter counter, RecipesRepresentationMapper recipesRepresentationMapper, IngredientService ingredientService, IngredientRepository ingredientRepository) {
        this.recipesRepository           = recipesRepository;
        this.counter                     = counter;
        this.recipesRepresentationMapper = recipesRepresentationMapper;
        this.ingredientService           = ingredientService;
        this.ingredientRepository        = ingredientRepository;
    }

    public boolean deleteById(String id) {
        if(this.getById(id).isPresent()) {
            recipesRepository.deleteById(id);
            return true;
        }
        else return false;
    }

    public List<Recipes> getAllRecipes() {
        return (List<Recipes>) recipesRepository.findAll();
    }

    public Optional<Recipes> getById(String id) {
        return recipesRepository.findById(id);
    }

    @Transactional
    public Optional<Recipes> addARecipe(NewRecipesRepresentation recipes) {
        Set<NewMesureRepresentation> listNewMesures = recipes.getListOfIngredients();
        Set<Mesure> listOfMesures = new HashSet<>();
        String idRecipe = this.counter.getId();
        for (NewMesureRepresentation mesure : listNewMesures) {
            String isIngredient = mesure.getIngredient();
            if(this.ingredientService.nameExists(isIngredient)) {
                Ingredient ingredient = this.ingredientRepository.findByName(isIngredient).get();
                Mesure saveMesure = new Mesure(
                        new MesureKey(ingredient.getId(), idRecipe ),
                        mesure.getUnit(), mesure.getQuantity());
                listOfMesures.add(saveMesure);
            } else {
                String ingredientId = counter.getId();
                this.ingredientRepository.save(new Ingredient(ingredientId ,mesure.getIngredient()));
                Mesure saveMesure = new Mesure(
                        new MesureKey(ingredientId, idRecipe),
                        mesure.getUnit(), mesure.getQuantity());
                listOfMesures.add(saveMesure);
            }
        }
        Recipes recipe = new Recipes(
                idRecipe,
                recipes.getName(),
                listOfMesures
        );
        this.recipesRepository.save(recipe);
        return Optional.of(recipe);
    }

    /*public Recipes updateARecipe(String id, NewRecipesRepresentation recipes) {
        Optional<Recipes> referencesRecipe = recipesRepository.findById(id);
        if(referencesRecipe.isPresent()) {
            if (recipes.getName() != null) { referencesRecipe.get().setName(recipes.getName()); }
            if(recipes.getRecipeContent() != null) { referencesRecipe.get().setRecipeContent(recipes.getRecipeContent()); }
            return recipesRepository.save(referencesRecipe.get());
        }
        else return new Recipes(id, recipes.getName(), recipes.getRecipeContent());
    }*/
}
