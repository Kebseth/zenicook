package com.example.zenicook.beans;

import com.example.zenicook.model.Ingredient;
import com.example.zenicook.model.Recipes;
import com.example.zenicook.repository.IngredientRepository;
import com.example.zenicook.repository.RecipesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class Convert {

    IngredientRepository ingredientRepository;
    RecipesRepository recipesRepository;

    @Autowired
    public Convert(IngredientRepository ingredientRepository, RecipesRepository recipesRepository) {
        this.ingredientRepository = ingredientRepository;
        this.recipesRepository    = recipesRepository;
    }

    public String convertIdInIngredientName(String id){
       return this.ingredientRepository.findById(id)
               .map(Ingredient::getName)
               .orElseThrow(() -> new RuntimeException("L'ingredient n'éxiste pas"));
    }

    public String convertIdInRecipeName (String id){
        return this.recipesRepository.findById(id)
                .map(Recipes::getName)
                .orElseThrow(() -> new RuntimeException("La recette n'éxiste pas"));
    }
}
