package com.example.zenicook.controller;

import com.example.zenicook.model.Recipes;
import com.example.zenicook.representation.recipe.NewRecipesRepresentation;
import com.example.zenicook.representation.recipe.RecipesRepresentation;
import com.example.zenicook.representation.recipe.RecipesRepresentationMapper;
import com.example.zenicook.representation.ResultRepresentation;
import com.example.zenicook.service.RecipesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/recipes")
public class RecipesController {
    RecipesService recipesService;
    RecipesRepresentationMapper recipesRepresentationMapper;

    @Autowired
    public RecipesController(RecipesService recipesService, RecipesRepresentationMapper recipesRepresentationMapper) {
        this.recipesService = recipesService;
        this.recipesRepresentationMapper = recipesRepresentationMapper;
    }

    @GetMapping("")
    public List<RecipesRepresentation> getAll() {
        return this.recipesService.getAllRecipes().stream()
                                                  .map(recipe -> recipesRepresentationMapper.mapToRecipe(recipe))
                                                  .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<RecipesRepresentation> getById(@PathVariable String id) {
        final Optional<Recipes> response = this.recipesService.getById(id);
        return response.map(recipes -> recipesRepresentationMapper.mapToRecipe(recipes))
                       .map(ResponseEntity::ok)
                       .orElseGet( () -> ResponseEntity.notFound().build());
    }

    /*@PutMapping("/{id}")
    public RecipesRepresentation updateById(@PathVariable String id, @RequestBody NewRecipesRepresentation recipes) {
        return this.recipesRepresentationMapper.mapToRecipe(recipesService.updateARecipe(id, recipes));
    }*/

    @DeleteMapping("/{id}")
    public ResultRepresentation deleteById(@PathVariable String id) {
        boolean result = recipesService.deleteById(id);
        return new ResultRepresentation(result);
    }

    @PostMapping
    public ResponseEntity<RecipesRepresentation> createRecipe(@RequestBody NewRecipesRepresentation body) {
        return this.recipesService.addARecipe(body)
                .map(recipes -> recipesRepresentationMapper.mapToRecipe(recipes))
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
