package com.example.zenicook.controller;

import com.example.zenicook.exception.IngredientException;
import com.example.zenicook.representation.ingredient.IngredientRepresentation;
import com.example.zenicook.representation.ingredient.IngredientRepresentationMapper;
import com.example.zenicook.representation.ingredient.NewIngredientRepresentation;
import com.example.zenicook.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ingredients")
public class IngredientController {
    IngredientService ingredientService;
    IngredientRepresentationMapper ingredientRepresentationMapper;

    @Autowired
    public IngredientController(IngredientService ingredientService, IngredientRepresentationMapper ingredientRepresentationMapper) {
        this.ingredientService = ingredientService;
        this.ingredientRepresentationMapper = ingredientRepresentationMapper;
    }

    @GetMapping("")
    public List<IngredientRepresentation> getAllIngredients() {
        return this.ingredientService.getAllIngredient().stream()
                .map(this.ingredientRepresentationMapper::mapToIngredient)
                .collect(Collectors.toList());
    }

    @PostMapping
    public IngredientRepresentation createIngredient(@RequestBody NewIngredientRepresentation body) {
        try {
            return ingredientRepresentationMapper.mapToIngredient(ingredientService.addAnIngredient(body));
        } catch (IngredientException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getText());
        }
    }

    @PutMapping("/{id}")
    public IngredientRepresentation updateById(@PathVariable String id, @RequestBody NewIngredientRepresentation ingredient) {
        return this.ingredientRepresentationMapper.mapToIngredient(ingredientService.updateAnIngredient(id, ingredient));
    }
}
