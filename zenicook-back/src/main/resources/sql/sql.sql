drop table if exists mesures;
drop table if exists recipes;
drop table if exists ingredient;

create table recipes
(
    id text primary key,
    name text not null
);

create table ingredient
(
    id text primary key ,
    name text not null
);

create table mesures
(
    quantity int not null,
    recipe_id text REFERENCES recipes (id),
    ingredient_id text REFERENCES ingredient (id),
    unit text,
    PRIMARY KEY(ingredient_id, recipe_id)
);

insert into ingredient (id, name)
values
('e7aa3ce4-db0a-4c86-b9cc-a2174f2e54df', 'oeuf'),
('fdc22351-e085-4d90-aeaa-e3af0325377b', 'jambon'),
('1ce5888b-a28d-4de7-a650-fb44e9f36b45', 'fromage'),
('b5717f24-3578-481e-9819-da8045b19323', 'galette');

insert into recipes (id, name)
values ('4d383fac-0941-4f36-967a-fedbf07fefcb', 'galettes complètes');

insert into mesures(quantity, recipe_id, ingredient_id)
values
(2, '4d383fac-0941-4f36-967a-fedbf07fefcb', 'e7aa3ce4-db0a-4c86-b9cc-a2174f2e54df'),
(1, '4d383fac-0941-4f36-967a-fedbf07fefcb', 'b5717f24-3578-481e-9819-da8045b19323');


insert into mesures(quantity, recipe_id, ingredient_id, unit)
values
(1, '4d383fac-0941-4f36-967a-fedbf07fefcb', 'fdc22351-e085-4d90-aeaa-e3af0325377b', 'tranche'),
(200, '4d383fac-0941-4f36-967a-fedbf07fefcb', '1ce5888b-a28d-4de7-a650-fb44e9f36b45', 'grammes')