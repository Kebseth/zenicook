export class RecipeModel {
  id: string;
  name: string;
  recipeContent: string;

  constructor(id: string, name: string, recipeContent: string) {
    this.id = id;
    this.name = name;
    this.recipeContent = recipeContent;
  }
}
