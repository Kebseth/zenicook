import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { ListRecipeComponent } from './Component/list-recipe/list-recipe.component';
import { RecipeComponent } from './Component/recipe/recipe.component';
import {ZenicookService} from './service/zenicookService';
import { HeaderComponent } from './Component/header/header.component';
import { IngredientComponent } from './ingredient/ingredient.component';

const appRoutes: Routes = [
  { path: 'recipes', component: ListRecipeComponent },
  { path: '',
    redirectTo: 'recipes',
    pathMatch: 'full'
  },
  { path: 'ingredients', component: IngredientComponent},
  { path: '**', component: ListRecipeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ListRecipeComponent,
    RecipeComponent,
    HeaderComponent,
    IngredientComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )

  ],
  providers: [
    ZenicookService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
