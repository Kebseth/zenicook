import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RecipeModel} from '../model/recipe.model';
import {IngredientModel} from '../model/ingredient.model';

@Injectable()
export class ZenicookService {

  constructor(private http: HttpClient) {}

  getRecipes(): Observable<RecipeModel[]> {
    return this.http.get<RecipeModel[]>('http://localhost:8080/recipes');
  }

  getIngredients(): Observable<IngredientModel[]> {
    return this.http.get<IngredientModel[]>('http://localhost:8080/ingredients');
  }
}
