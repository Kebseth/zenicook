import {Component, OnInit} from '@angular/core';
import {IngredientModel} from '../model/ingredient.model';
import {ZenicookService} from '../service/zenicookService';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent implements OnInit {

  listOfIngredients: IngredientModel[];

  constructor(private zenicookService: ZenicookService) {
  }

  ngOnInit() {
    this.getAllRecipes();
  }

  getAllRecipes() {
    this.zenicookService.getIngredients().subscribe(json => {
      console.log(json);
    });
  }

}
