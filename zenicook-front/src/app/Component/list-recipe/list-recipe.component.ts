import {Component, OnInit} from '@angular/core';
import {RecipeModel} from '../../model/recipe.model';
import {ZenicookService} from '../../service/zenicookService';

@Component({
  selector: 'app-list-recipe',
  templateUrl: './list-recipe.component.html',
  styleUrls: ['./list-recipe.component.css']
})
export class ListRecipeComponent implements OnInit {

  listRecipes: RecipeModel[];

  constructor(private zenicookService: ZenicookService) {
  }

  ngOnInit() {
    this.getAllRecipes();
  }

  getAllRecipes() {
    this.zenicookService.getRecipes().subscribe(json => {
      this.listRecipes = json;
    });
  }

}
